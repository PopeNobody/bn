cpp_first:=
MAKEFLAGS:=rR -j1
include etc/tools.mk

all:

CXXFLAGS:=@etc/cxxflags
CPPFLAGS=$(CXXFLAGS)
ASMFLAGS:=@etc/asmflags
LNKFLAGS:=@etc/lnkflags
LNKLIBS:=@etc/lnklibs
dep_flags = -MD  -MT $@ -MF $(@:.ii=.dd) -MP


bin_exe=  source_location dn rbn
bin_run= $(patsubst %, run/%, $(bin_exe))
run/%: % all
	./$<

bin_src=  $(patsubst     %, %.cc, $(bin_exe))
all_src=  $(wildcard     *.cc)
lib_src=  $(filter-out $(bin_src), $(all_src))
bin_obj=  $(patsubst     %.cc,  %.oo,  $(bin_src))
all_dep=  $(patsubst %.cc,%.dd,$(all_src))
all_cpp=  $(patsubst %.cc, %.ii, $(all_src))

#$(foreach var,lib_src all_src bin_src,$(warning $(var)=$($(var))))


include /dev/null $(wildcard deps.all.mk)

deps.all deps.all.mk: $(all_dep)
	perl etc/alldeps.pl $(all_dep)

tags: deps.all etc/alldeps.pl
	ctags $$(cat deps.all)

ifneq ($(lib_src),)
lib_obj=  $(patsubst     %.cc,  %.oo,  $(lib_src))

LNKLIBS+=liblib.aa

#    liblib.aa: liblib(%): %:
#    	ar cr liblib.aa $<

(%): %
	ar cr $@ $<

liblib.aa: $(patsubst %,liblib.aa(%),$(lib_obj))

endif
%.dd: %.ii
	@echo made $@

%.ii: %.cc etc/cxxflags
	@mkdir -p $(dir $@) && rm -f $@
	$(CXX) $(CXXFLAGS) $< -o $(<:.cc=.ii) -E  $(dep_flags)

%.oo: %.ii etc/cxxflags
	@mkdir -p $(dir $@) && rm -f $@
	if ! $(CXX) $(CXXFLAGS) $< -o $@ -c  $(dep_flags); then rm -f $<; exit 1; fi


$(bin_exe): %: %.oo etc/lnkflags liblib.aa
	$(CXX) $(LNKFLAGS) -o $@ $<  $(LNKLIBS) $(dep_flags)

clean:
	rm -f $(bin_exe)
	rm -f *.oo *.ii *.dd tags deps.all deps.all.mk *.aa


.PRECIOUS: $(all_src) $(all_gen) $(all_dep) $(all_cpp)

all: $(bin_exe)

.PHONY: all clean

etc/tools.mk:;
