#error not ready yet
#pragma once
struct stringify_t {
  mutable ostringstream txt;
  ostream &str;

  template<typename rhs_t>
    friend ostream &operator<<(const stringify_t&lhs, const rhs_t &rhs)
    {
      return lhs.str<<rhs;
    }
  stringify_t(ostream &str=std::cout)
    :str(str)
  {
  }
  ~stringify_t() {
    str<<(const string)*this;
  }
  operator const string() const {
    string res = txt.str();
    txt.str("");
    return res;
  }
  operator ostream &() const {
    return str;
  }
};
