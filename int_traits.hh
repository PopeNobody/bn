#pragma once
#include "std.hh"

template<typename _type>
struct next_t;

template<>
struct next_t<int8_t> { typedef int16_t type; };
template<>
struct next_t<int16_t> { typedef int16_t type; };
template<>
struct next_t<int32_t> { typedef int64_t type; };
template<>
struct next_t<uint8_t> { typedef uint16_t type; };
template<>
struct next_t<uint16_t> { typedef uint32_t type; };
template<>
struct next_t<uint32_t> { typedef uint64_t type; };

template<typename _type>
struct int_traits_t
{
  typedef _type type;
  typedef int_traits_t<type> self;
  typedef typename next_t<type>::type next;
  typedef typename next_t<next>::type nextnext;
  static constexpr type mask = (type)-1;
  static constexpr next oflow = next(mask)+1;
  static constexpr size_t bin_wid = 8*sizeof(type);
  static constexpr size_t hex_wid = bin_wid/4;
};
