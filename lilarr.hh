// License Info Moved to Eof  /COPYRIGHT
#pragma once
#include "std.hh"
#include "xassert.hh"

namespace util_ns {
    using std::uint16_t;
    template<class T, uint16_t N>
    class arr_t {
      public:
        T elems[short(N)];    // fixed-size array of elements of type T

      public:
        // type definitions
        typedef T              value_type;
        typedef T*             iterator;
        typedef const T*       const_iterator;
        typedef T&             reference;
        typedef const T&       const_reference;
        typedef std::uint16_t    uint16_type;
        typedef std::ptrdiff_t difference_type;

        // iterator support
        iterator        begin()       { return elems; }
        const_iterator  begin() const { return elems; }
        const_iterator cbegin() const { return elems; }
        
        iterator        end()       { return elems+N; }
        const_iterator  end() const { return elems+N; }
        const_iterator cend() const { return elems+N; }

        // reverse iterator support
        typedef std::reverse_iterator<iterator> reverse_iterator;
        typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

        reverse_iterator rbegin() { return reverse_iterator(end()); }
        const_reverse_iterator rbegin() const {
            return const_reverse_iterator(end());
        }
        const_reverse_iterator crbegin() const {
            return const_reverse_iterator(end());
        }

        reverse_iterator rend() { return reverse_iterator(begin()); }
        const_reverse_iterator rend() const {
            return const_reverse_iterator(begin());
        }
        const_reverse_iterator crend() const {
            return const_reverse_iterator(begin());
        }

        // operator[]
        reference operator[](uint16_type i) 
        { 
            xassert( i < N );
            return elems[i]; 
        }
        
        constexpr const_reference operator[](uint16_type i) const 
        {     
            xassert( i < N );
            return elems[i]; 
        }

        // at() with range check
        reference                           at(uint16_type i)       { return rangecheck(i), elems[i]; }
        constexpr const_reference at(uint16_type i) const { return rangecheck(i), elems[i]; }
    
        // front() and back()
        reference front() 
        { 
            return elems[0]; 
        }
        
        constexpr const_reference front() const 
        {
            return elems[0];
        }
        
        reference back() 
        { 
            return elems[N-1]; 
        }
        
        constexpr const_reference back() const 
        { 
            return elems[N-1]; 
        }

        // size is constant
        static constexpr uint16_type size() { return N; }
        static constexpr bool empty() { return false; }
        static constexpr uint16_type max_size() { return N; }
        enum { static_size = N };

        // swap (note: linear complexity)
        void swap (arr_t<T,N>& y) {
            for (uint16_type i = 0; i < N; ++i)
                std::swap(elems[i],y.elems[i]);
        }

        // direct access to data (read-only)
        const T* data() const { return elems; }
        T* data() { return elems; }

        // use array as C array (direct read/write access to data)
        T* c_array() { return elems; }

        // assignment with type conversion
        template <typename T2>
        arr_t<T,N>& operator= (const arr_t<T2,N>& rhs) {
            std::copy(rhs.begin(),rhs.end(), begin());
            return *this;
        }

        // assign one value to all elements
        void assign (const T& value) { fill ( value ); }    // A synonym for fill
        void fill   (const T& value)
        {
            std::fill_n(begin(),size(),value);
        }

        // check range (may be private because it is static)
        static constexpr bool rangecheck (uint16_type i) {
            return i >= size() ? 
              throw std::out_of_range ("array<>: index out of range"): true;
        }

    };

    // comparisons
    template<class T,uint16_t N>
    bool operator== (const arr_t<T,N>& x, const arr_t<T,N>& y) {
        return
          std::equal(x.begin(), x.end(), y.begin());
    }
    template<class T, std::uint16_t N1, std::uint16_t N2>
      std::strong_ordering 
      operator<=> (const arr_t<T,N1> &l, const arr_t<T,N2> r)
      {
        const uint16_t min_n=std::min(N1,N2);
        std::strong_ordering res(0<=>0);
        for(uint16_t n=0;n<min_n;n++){
          res=l[n]<=>r[n];
          if(res!=0)
            return res;
        }
        return N1<=>N2;
      }

    // global swap()
    template<class T, std::uint16_t N>
    inline void swap (arr_t<T,N>& x, arr_t<T,N>& y) {
        x.swap(y);
    }

    
    // Const version.
    template <typename T, std::uint16_t N>
    const T(&get_c_array(const arr_t<T,N>& arg))[N]
    {
        return arg.elems;
    }
    
    template <class It> std::uint16_t hash_range(It, It);

    template<class T, std::uint16_t N>
    std::uint16_t hash_value(const arr_t<T,N>& arr)
    {
        return hash_range(arr.begin(), arr.end());
    }

   template <uint16_t Idx, typename T, uint16_t N>
   T &get(util_ns::arr_t<T,N> &arr)  {
       xassert ( Idx < N );
       return arr[Idx];
       }
    
   template <uint16_t Idx, typename T, uint16_t N>
   const T &get(const arr_t<T,N> &arr)  {
       xassert ( Idx < N );
       return arr[Idx];
       }

}

namespace std {
   template <uint16_t Idx, typename T, uint16_t N>
   T &get(util_ns::arr_t<T,N> &arr)  {
       xassert ( Idx < N );
       return arr[Idx];
       }
    
   template <uint16_t Idx, typename T, uint16_t N>
   const T &get(const util_ns::arr_t<T,N> &arr)  {
       xassert ( Idx < N );
       return arr[Idx];
       }
}


//// COPYRIGHT

/* The following code declares class array,
 * an STL container (as wrapper) for arrays of constant size.
 *
 * See
 *      http://www.boost.org/libs/array/
 * for documentation.
 *
 * The original author site is at: http://www.josuttis.com/
 *
 * (C) Copyright Nicolai M. Josuttis 2001.
 *
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 *
 *  9 Jan 2013 - (mtc) Added constexpr
 * 14 Apr 2012 - (mtc) Added support for boost::hash
 * 28 Dec 2010 - (mtc) Added cbegin and cend (and crbegin and crend) for C++Ox compatibility.
 * 10 Mar 2010 - (mtc) fill method added, matching resolution of the standard library working group.
 *      See <http://www.open-std.org/jtc1/sc22/wg21/docs/lwg-defects.html#776> or Trac issue #3168
 *      Eventually, we should remove "assign" which is now a synonym for "fill" (Marshall Clow)
 * 10 Mar 2010 - added workaround for SUNCC and !STLPort [trac #3893] (Marshall Clow)
 * 29 Jan 2004 - c_array() added, BOOST_NO_PRIVATE_IN_AGGREGATE removed (Nico Josuttis)
 * 23 Aug 2002 - fix for Non-MSVC compilers combined with MSVC libraries.
 * 05 Aug 2001 - minor update (Nico Josuttis)
 * 20 Jan 2001 - STLport fix (Beman Dawes)
 * 29 Sep 2000 - Initial Revision (Nico Josuttis)
 *
 * Jan 29, 2004
 */
