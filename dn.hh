#include "std.hh"
#include "sl_t.hh"
#include "xassert.hh"

struct dn_b {
  uint64_t lp;
  uint64_t hp;
  dn_b(const uint64_t &lp=0, const uint64_t &hp=0);
  dn_b(const dn_b &rhs);
  dn_b(const string &rhs);
};
struct dn : public dn_b {
  static constexpr uint64_t mask=uint64_t(0)-1;
  static constexpr uint64_t hb=mask-mask/2;
  using dn_b::dn_b;
  string str() {
    ostringstream str;
    str<<setw(4*sizeof(lp))<<setfill('0')<<hex<<hp<<lp;
    string res=str.str();
    size_t p=res.find_first_not_of("0");
    if(p==string::npos)
      return "0x00";
    if(p%1)
      --p;
    return "0x"+res.substr(p);
  }
  static dn add(dn const&lhs, dn const&rhs) {
    dn res(lhs);
    res.lp+=rhs.lp;
    res.hp+=((res.lp & hb) && (rhs.lp & hb));
    bool oflow= ((res.hp & hb) && (rhs.hp & hb));
    if(oflow)
      throw runtime_error("overflow: add");
    return res;
  }
  friend dn operator+(dn const&lhs, dn const&rhs) {
    return dn::add(lhs,rhs);
  }
  friend auto operator<=>(dn const&lhs, dn const&rhs) {
    auto res = lhs.hp <=> rhs.hp;
    if(res==0)
      res=lhs.lp <=> rhs.lp;
    return res;
  }
  friend auto operator==(dn const&lhs, dn const&rhs) {
    return lhs<=>rhs == 0;
  }
};

