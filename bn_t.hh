#include "std.hh"
#include <cmath>
#include "lilarr.hh"
#include "int_traits.hh"
#include "sl_t.hh"
#include "xassert.hh"

using namespace std;
#ifdef assert
#undef assert
#endif
#define assert xassert


struct dn;
struct bn_t : public int_traits_t<uint8_t> {
  typedef next tmp_t;
  //val_i;
  struct dat_t : public vector<type> {
    using vector<type>::vector;
    using vector<type>::operator=;
    void push_front(type val)
    {
      insert(begin(),val);
    }
  };
  typedef dat_t::const_iterator itr_fc;
  typedef dat_t::iterator       itr_fm;
  typedef std::reverse_iterator<itr_fm> itr_rm;
  typedef std::reverse_iterator<itr_fc> itr_rc;
  typedef type * data_t;
  data_t data;
  const char *txt;
  string txt_str;
  bn_t(const dn &val);
  bn_t(const string &val) {
    *this=from(val);
  };
  static inline char to_dig(char ch) {
    assert(ch<0x10);
    return (ch<10)?(ch+'0'):(ch+('a'-10));
  }
  static inline char fr_dig(int ch) {
    assert(isxdigit(ch));
    ch=tolower(ch);
    return isalpha(ch)?ch-'a'+10:ch-'0';
  }
  bn_t(uint64_t val)
  {
    xassert(val>=0);
    while(val) {
      dat.push_back(val&mask);
      val>>= bin_wid;
    }
    normalize();
  }
  bn_t()
  {
  }
  // The const is a lie! 
  void normalize() const {
    ((bn_t*)this)->normalize();
  }
  void normalize() {
    static bool recurse=false;
    if(recurse)
      return;
    recurse=true;
    if(size()&&!dat.back()) {
      while(size()){
        if(dat.back())
          break;
        dat.pop_back();
      }
      txt_str=c_str();
    }
    data=dat.data();
    recurse=false;
  }
  dat_t dat;

  const char *c_str() const {
    static struct {
      char buf[1020];
      char min[3];
      char end[1];
    } xxx;
    normalize();
    auto b(dat.begin()), e(dat.end());
    if(b==e)
      return "0x00";
    char*p = xxx.end;
    *p=0;
    tmp_t d;
    while(b!=e) {
      d=1;
      d<<=bin_wid;
      d+=(*b++&mask); 
      while(d>1) {
        *--p=to_dig((d/0x01)%0x10);
        d/=0x10;
      }
    }
    if((xxx.end-p)%2)
      *--p='0';
    *--p='x';
    *--p='0';
    return p;
  }
  string str() {
    return c_str();
  }
  ostream &write(ostream &rhs) const
  {
    return rhs<<c_str();
  }

  size_t size() const {
    return dat.size();
  };
  friend bool operator==(const bn_t &lhs, const bn_t &rhs) 
  {
    rhs.normalize();
    size_t size=lhs.size();
    //       show(lhs);
    //       show(rhs);
    bool res=true;
    if(size!=rhs.size()) {
      //         show(size);
      //         show(rhs.size());
      //         show(size!=rhs.size());
      res=false;
    } else {
      for(size_t i=0;res&&i<size;i++)
        if(lhs.dat[i]!=rhs.dat[i])
          res=false;
    }
    //       show(res);
    return res; 
  }
  friend strong_ordering operator<=>(const bn_t &lhs, const bn_t &rhs) 
  {
    lhs.normalize();
    rhs.normalize();
    auto res = lhs.size()<=>rhs.size();
    //       show(res!=0);
    //       show(lhs.size()==0);
    if(res!=0 || lhs.size()==0)
      return res;
    size_t size=lhs.size();
    for(size_t i=size-1;i;i--) {
      res=lhs.dat[i] <=> rhs.dat[i];
      if(res!=0)
        return res;
    }
    return strong_ordering::equal;
  }
  //     friend bn_t &operator*=(bn_t &lhs, bn_t const& rhs);
  //     friend bn_t &operator+=(bn_t &lhs, bn_t const& rhs);
  //     bn_t &operator*=(const bn_t &rhs);
  static bn_t from(const string &val);
  friend
    bn_t operator*(const bn_t &lhs, const bn_t &rhs);
  //     bn_t &operator+=(const bn_t &rhs);
  friend
    bn_t operator+(const bn_t &lhs, const bn_t &rhs);
  //     bn_t &operator-=(const bn_t &rhs);
  friend
    bn_t operator-(const bn_t &lhs, const bn_t &rhs);
  //     bn_t &operator-=(const bn_t &rhs);
  friend
    bn_t operator/(const bn_t &lhs, const bn_t &rhs);
  //     bn_t &operator/=(const bn_t &rhs);
  friend
    bn_t operator%(const bn_t &lhs, const bn_t &rhs);
  //     bn_t &operator%=(const bn_t &rhs);
  static const bn_t zero;
  bool operator!() const {
    bool bizarro=*this==zero;
    //       show(bizarro);
    return bizarro;
  }
  friend
    ostream &operator << (ostream &lhs, const bn_t rhs);
  static void div(bn_t const&lhs, bn_t const &rhs,
      bn_t &q, bn_t &r);
  static void mul(bn_t const &src, uint8_t ch, bn_t &dst)
  {
    dst=src;
    auto &dat=dst.dat;
    if(dat.size()==0){
      dat.push_back(ch);
    } else {
      size_t p=0;
      tmp_t acc=tmp_t(ch)*0x100;
      while(acc) {
        acc/=0x100;
        if(p<dat.size()) {
          acc+=dat[p];
          dat[p]=acc&mask;
        } else {
          dat.push_back(acc&mask);
        }
      }
    }
  }
};
const bn_t bn_t::zero;
//   const bn_t bn_t::zero;
auto find_hex(const string & val)
{
  auto beg(val.begin()), end(val.end());
  while(beg!=end && isspace(*beg))
    ++beg;
  while(beg!=end && isspace(*(end-1)))
    --end;
  if(end-beg >1 && beg[0]=='0' && beg[1]=='x')
    beg+=2;
  if(beg==end)
    throwRe("no hex number found in: " << val);
  while(beg!=end && *beg=='0')
    beg++;
  auto pos=beg;
  while(beg!=end) {
    if(!isxdigit(*beg++)) {
      throwRe("bad data found at pos(" << (beg-val.begin()) << ") in str: "
          << val);
    };
  }
  return make_pair(
      make_reverse_iterator(&*end),
      make_reverse_iterator(&*pos));
}
//     while(b!=e) {
//       cout << *b++ << endl;
//     }
  //     while(b!=e){
  //       tmp_t acc;
  //       bn_t res;
  //       while(beg!=end) {
  //         type dig=fr_dig(*beg++);
  //         if(beg!=end)
  //           dig=(dig*0x10)+fr_dig(*beg++);
  //         res.dat.push_back(dig);
  //       }
  //       show(res);
ostream &operator << (ostream &lhs, const bn_t rhs) 
{
  return rhs.write(lhs);
}
bn_t operator*(const bn_t &lhs, const bn_t &rhs) {
  if(!rhs || lhs==1)
    return rhs;
  if(!lhs || rhs==1)
    return lhs;
  vector<bn_t> col;
  for(size_t i=0;i<lhs.dat.size();i++){
    if(lhs.dat[i]) {
      bn_t add;
      for(size_t j=0;j<i;j++)
        add.dat.push_back(0);
      bn_t::tmp_t tmp;
      for(size_t j=0;j<rhs.dat.size();j++) {
        tmp/=0x100;
        tmp+=lhs.dat[i]*rhs.dat[j];
      }
    }
  }
  bn_t sum;
  for(auto const &num : col) {
    sum=sum+num;
  }
  return sum;
}
//   bn_t &operator%(const bn_t &lhs, const bn_t &rhs) {
//     auto &self=*this;
//     return self;
//   }
void bn_t::div(const bn_t &lhs, const bn_t &rhs,
          bn_t &q, bn_t &r)
{
  size_t i=0;
  // there are a lot of good ways to do this job.
  //
  // this is not one of them.
  //
  bn_t tmp;
//     show(lhs);
  while(tmp<lhs) {
    show(++i);
    show(tmp=tmp+rhs);
    show(lhs);
    show(tmp<lhs);
  }
  show(lhs);
  if(lhs<tmp){
    --i;
    tmp=tmp-rhs;
  }
  r=r-tmp;
  q=i;
}
bn_t operator%(const bn_t &lhs, const bn_t &rhs) {
  bn_t q,r;
  bn_t::div(lhs,rhs,q,r);
  return q;
}
bn_t operator/(const bn_t &lhs, const bn_t &rhs) {
  bn_t q,r;
  bn_t::div(lhs,rhs,q,r);
  return q;
}
bn_t operator+(const bn_t &lhs, const bn_t &rhs) {
  static bool verbose=false;
  bn_t res;
  if(verbose)
    cout << "operator+( " << lhs << " , " << rhs << " )\n";
  size_t pos;
  size_t com=min(lhs.size(),rhs.size());
  bn_t::tmp_t tmp=0;
  for(pos=0;pos<com;pos++) {
    show(pos);
    show(tmp);
    bn_t::tmp_t l=lhs.dat[pos];
    show(l);
    bn_t::tmp_t r=rhs.dat[pos];
    show(l);
    tmp+=l;
    show(tmp);
    tmp+=r;
    show(tmp);
    res.dat.push_back(tmp&bn_t::mask);
    tmp/=0x100;
    show(tmp);
  }
  auto &long_one=(lhs.size()<rhs.size())?rhs:lhs;
  while(pos<long_one.size()){
    tmp+=long_one.dat[pos++];
    res.dat.push_back(tmp&bn_t::mask);
    tmp>>=bn_t::bin_wid;
  }
  show(tmp);
  show(res);
  if(tmp)
    res.dat.push_back(tmp&bn_t::mask);
  show(res);

  res.normalize();
  return res;
}
bn_t operator-(const bn_t &lhs, const bn_t &rhs) {
  bn_t res=lhs;
  if(res.size()<rhs.size()) {
    while(res.size()<rhs.size())
      res.dat.push_back(0);
  }
  bn_t::tmp_t tmp=0;
  --tmp;
  size_t pos;
  for(pos=0;pos<rhs.size();pos++) {
    if(pos<lhs.size())
      tmp+=lhs.dat[pos];
    tmp-=rhs.dat[pos];
    res.dat[pos]=tmp&bn_t::mask;
    tmp>>=bn_t::bin_wid;
  }
//     show(tmp);
  if(tmp)
    res.dat.push_back(tmp&bn_t::mask);

  res.normalize();
  return res;
}
//   bn_t::operator bool() const {
//     auto b(dat.begin()), e(dat.end());
//     if(b==e)
//       return false;
//     do {
//       if(*b)
//         return false;
//     } while(++b!=e);
//     return true;
//   }

bn_t bn_t::from(const string &val) {
  auto its = find_hex(val);
  vector<uint8_t> dat;
  for( auto b(its.first),e(its.second); b!=e;)
  {
    type dif = fr_dig(*b++);
    if(b!=e)
      dif=dif+0x10*fr_dig(*b++);
    dat.push_back(dif);
  }
  bn_t res;
  res.dat=dat;
  res.normalize();
  return res;
}
int xmain();
static const string str1 = string("0x0100");
int tmp_test() {
  uint64_t X=0xffffff0;
  bn_t x=0xffffff0;
  uint64_t Z=0x1ff;
  bn_t z=0x1ff;
  for(int i=1;i<100;i++) {
    show(i);
    show(x=x+z);
    show(X=X+Z);
    try { 
      assert_op(x,==,X);
      uint64_t tmp=3*Z;
      assert((Z+tmp)-tmp==Z);
      Z+=tmp;
      z=Z;
    } catch ( ... ) {
      show(log(Z)/log(2));
      show(i);
      show(x);
      show(X);
      X+Z;
      x+z;
      break;
    }
  };
  return 0;
}
int main() {
  tmp_test();
  return xmain();
};
void str_tests();
void op_tests();
int xmain() {
  cout << hex << showbase << boolalpha;
  str_tests();
  op_tests();
  return 0;
}
void op_tests()
{
}
struct case_t {
  uint32_t init;
  char str[16];
  size_t size;
};
vector<case_t> cases = {
  { 0, "0x00", 0 },
  { 1, "0x01", 1 },
  {  0x10,        "0x10",        1  },
  {  0x100,       "0x0100",      2  },
  {  0x1000,      "0x1000",      2  },
  {  0x10000,     "0x010000",    3  },
  {  0x100000,    "0x100000",    3  },
  {  0x1000000,   "0x01000000",  4  },
  {  0x10000000,  "0x10000000",  4  },
  {  0x1000000f,  "0x1000000f",  4  },
  {  0xdeadbeef,  "0xdeadbeef",  4  },
  {  0xbaadf00d,  "0xbaadf00d",  4  },
};
ostream &operator<<(ostream &lhs, const case_t &rhs) {
  lhs  << setw(sizeof(rhs.str)) << rhs.init
       << setw(sizeof(rhs.str)) << rhs.str
       << setw(3) << rhs.size;
  return lhs;
}
void str_tests() {
  bn_t def;
  for( auto c : cases ) {
    bn_t bn(c.init);
    assert((c.init <=> bn)==0);
    assert_op(c.str,==,bn.str());
    bn_t sn(c.str);
    assert_op(bn.str(),==,sn.str());
    assert(bn.str()==sn.str());
    assert_op(bn,==,sn);
    assert(bn<=>sn==0);
    assert(bn==sn);
    assert(bn<0 == false);
    assert(c.size == bn.size());
    if(c.init) {
      assert(def != bn);
      assert(bn>0);
      assert(bn>=0);
      assert(bn<=0 == false);
    } else {
      assert(def == bn);
      assert(bn>0 == false);
      assert(bn>=0);
      assert(bn<0 == false);
      assert(bn<=0);
      assert(bn>=0);
    }
  }
}
//   bn_t bn_t::from(const string &val) {
//     auto its = find_hex(val);
//     vector<uint8_t> dat;
//     for( auto b(its.first),e(its.second); b!=e;)
//     {
//       type dif = fr_dig(*b++);
//       if(b!=e)
//         dif=dif+0x10*fr_dig(*b++);
//       dat.push_back(dif);
//     }
//     bn_t res;
//     res.dat=dat;
//     res.normalize();
//     return res;
//   }
