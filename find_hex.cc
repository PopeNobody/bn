#include "std.hh"

auto find_hex(const string & val)
{
  auto beg(val.begin()), end(val.end());
  while(beg!=end && isspace(*beg))
    ++beg;
  while(beg!=end && isspace(*(end-1)))
    --end;
  if(end-beg >1 && beg[0]=='0' && beg[1]=='x')
    beg+=2;
  if(beg==end)
    throwRe("no hex number found in: " << val);
  while(beg!=end && *beg=='0')
    beg++;
  auto pos=beg;
  while(beg!=end) {
    if(!isxdigit(*beg++)) {
      throwRe("bad data found at pos(" << (beg-val.begin()) << ") in str: "
          << val);
    };
  }
  return make_pair(
      make_reverse_iterator(&*end),
      make_reverse_iterator(&*pos));
}
