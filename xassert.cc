#include "xassert.hh"

//   printf 'using std::%s;\n' ostream ostringstream endl string setw 

using std::ostream;
using std::ostringstream;
using std::endl;
using std::string;
using std::setw;

ostream &operator<<(ostream &lhs, const sl_t &loc) {
  ostringstream str;
  str
    << endl
    << loc.file.data
    << ':' << loc.line
    << ':' << loc.column
    << ':' << loc.func.data
    << endl;
  assert(str.str().find("::")==string::npos);  
  return lhs;
};
void __xassert_fail(const char *cond, const char *file, unsigned line, 
    const char * func)
{
  using namespace std;
  ostringstream str;
  str
    << endl
    << file << ":" << line << ":"
    << func 
    << ":assertion failure" << endl
    << "   cond: " << cond
    << endl;
  cerr << str.str();
  throw runtime_error(str.str());
  exit(0);
}
void __xassert_fail(
    const char *cond, sl_t loc
    ) 
{
  __xassert_fail(cond, loc.file.data, loc.line, loc.func.data);
}
extern "C" {
  void (*__assert_fail)(const char *, const char *, unsigned,  const char *)
    = __xassert_fail;
}
