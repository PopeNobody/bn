#!/usr/bin/perl
# vim: ts=2 sw=2 ft=perl
eval 'exec perl -x -wS $0 ${1+"$@"}'
  if 0;

use strict;
use warnings;
use Util;
use Env qw($HOME @PATH);
$ENV{LANG}="C";
# Header installed by /opt/bin/mkperl
my @str;
for(my $i=0;$i<256;) {
  $_=lc(qquote(chr($i)).",      ");
  $_=substr($_,0,8);
  if(++$i % 8) {
    printf "%s",$_;
  } else {
    print;
  }
};
