#!/usr/bin/perl
# vim: ts=2 sw=2 ft=perl
eval 'exec perl -x -wS $0 ${1+"$@"}'
  if 0;

use strict;
use warnings;
use Util;
use vars qw( @todo ) ;
use Env qw($HOME @PATH);
$ENV{LANG}="C";
use Getopt::WonderBra;
@ARGV = getopt("gb",@_);
my %flags = ( g=>1, b=> 0 );
while(($_=shift(@ARGV))ne'--')
{
  die "expected dash" unless s{^-}{};
  die "expected falg" unless defined($flags{$_});
  $flags{$_}=!$flags{$_};
}
our(%ent);
sub fancy_files(@) {
  (@ent{ +qw( p f d ) }) = map m{((.*)/(.*))$};
  return \%ent;
}
push(@ARGV, suckdir("")) unless @ARGV;
while(@ARGV){
  for(shift){
    if(-e){
      next if m{\.git$|node_modules|CVS};
      next if m{\.[^/]*$};
      next if m{\.[ch][ch]?$};
      next if m{/boost$};
      next if m{(^|/)Makefile$};
      next if m{^etc/([a-z0-9A-Z_]+flags|lnklibs)$};
      if(-d) {
        s{/+$}{/};
        unshift(@ARGV, suckdir($_));
      } else {
        if( m{\.cc\.[ios][ios]$} ) {
          push(@todo,$_);
        }
        print "f $_";
      }
    } else {
      warn("$_ does not exist");
    }
  }
}
sub version {
  print "version 0.1.0";
};
sub help {
  print "in progress";
};
