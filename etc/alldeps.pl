#!/usr/bin/perl
# vim: ts=2 sw=2 ft=perl
eval 'exec perl -x -wS $0 ${1+"$@"}'
  if 0;

use strict;
use warnings;
use Util;
use Env qw($HOME @PATH);
$ENV{LANG}="C";
# Header installed by /opt/bin/mkperl
open(STDERR,">&STDOUT");
my (@deps,$src,$tgt,%list)=@ARGV;
#    (qx(
#      find *.d -type f -print
#      ));
@deps = map { split } @deps;
my @l;
my %t;
our($name);
sub dofile();
for $name (@deps) {
  # these are created when we preprocess a header, but
  # we still don't build headers.
  next if $name =~ m{hh\.d$};
  dofile();
}
sub dofile() {
  print STDERR "file: $name";
  chomp(@_ = suck($name));
  @_ = grep { !m{:$} } @_;
  $_=join("\n",@_);
  s{\\\n}{ }smg;
  @_=split('\n');
  @_=map { split /:/ } @_;
  $_=[split] for @_;
  for my $t (@{$_[0]}) {
    $t{$t}++;
    for my $s (@{$_[1]}) {
      $t{$s}++;
      push(@l,"tags $t: $s");
    };
  }
};
@l = grep { !m{o%} } @l;
push(@l, "tags: etc/alldeps.pl Makefile");
spit("deps.all.mk",join("\n",sort @l));
spit("deps.all", join("\n", grep { !m{oo$} } 
    sort keys %t));
