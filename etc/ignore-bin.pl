#!/usr/bin/perl
# vim: ts=2 sw=2 ft=perl
eval 'exec perl -x -wS $0 ${1+"$@"}'
  if 0;

use strict;
use warnings;
use Util;
use Env qw($HOME @PATH);
$ENV{LANG}="C";
my $dir=$ENV{PWD};
for(@ARGV) {
  chdir($_);
  my @src=glob("*.cc");
  @src=grep { s/.cc$// } @src;
  spit(".gitignore", join("\n", @src, ""));
};
