#!/usr/bin/perl
# vim: ts=2 sw=2 ft=perl
eval 'exec perl -x -wS $0 ${1+"$@"}'
  if 0;
open(STDERR,">&STDOUT");
use strict;
use warnings;
use Util;
use Env qw($HOME @PATH);
$ENV{LANG}="C";
# Header installed by /opt/bin/mkperl
my %buf;
my ($pid, $call, $str);
#    if(!open(STDIN,"|-")) {
#      open(STDOUT,">&STDERR");
#      exec(qw(strace -ooutput ./tst/t00_pipe));
#      die "exec: $!";
#    }
#    while(<STDIN>){
#      chomp;
#      print;
#    };
#    sleep 1;
@ARGV="output";
while(<ARGV>){
  chomp;
  if(s{<unfinished ...>$}{}){
    #15840 lseek(2, 0, SEEK_CUR <unfinished ...>
    ($pid, $call, $str) = m{^(\d+) ([^(]*)(.*)};
#        print "\n$_\n\t$pid\n\t$call\n\t$str\n";
    $buf{$pid}=[$call,$str];
  } elsif ( ($pid, $call, $str) = m{^(\d+) [<. ]+(\S+) resumed>(.*)} ) {
    #15840 <... read resumed>"", 65536)      = 0
#        print "\n$_\n\t$pid\n\t$call\n\t$str\n";
    die "mismatch" unless $buf{$pid}[0] eq $call;
    print "$pid $buf{$pid}[0] $buf{$pid}[1] (*******) $str";
  } else {
    print;
  };
}
