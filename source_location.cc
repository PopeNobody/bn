#include "std.hh"
#include "sl_t.hh"


//using source_location = std::source_location;
using source_location = std::experimental::source_location;



template<sl_t loc = sl_t()> // line 30
void print() {
  std::cout << loc << std::endl;
}

void caller_1() {
  print(); // line 36
}

void caller_2() {
  print(); // line 40
}

int main() {
  caller_1();
  caller_2();
  return 0;
}
