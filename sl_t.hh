#pragma once
#include "std.hh"
using source_location = std::experimental::source_location;


struct literal {
  char data[2048] = {'\0'}; // For simplicity, assume it is large enough to hold all possible values.
  constexpr literal(const char* src) {
    char* dst = &data[0];
    while(*src) *(dst++) = *(src++);
    *dst = '\0';
  }
};

struct sl_t {
  literal file;
  literal func;
  unsigned line;
  unsigned column;
  constexpr sl_t(
      const char* file, 
      const char* func, 
      unsigned line, 
      unsigned column)
    : file(file), func(func), line(line), column(column)
  {
  }

  constexpr sl_t(
      const source_location& loc = source_location::current())
    : sl_t::sl_t(loc.file_name(),
        loc.function_name(),
        loc.line(),
        loc.column()) 
  {
  }
  friend 
    std::ostream &operator<<(std::ostream &lhs, const sl_t &loc);
};
