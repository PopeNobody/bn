#error maybe useful one day
struct qchar_t {
  char buf[5];
  char *operator()(char ch) {
    if(!isprint(ch)) {
      snprintf(buf,sizeof(buf),"\\x%02x", ch);
      buf[4]=0;
    } else if(ch=='"') {
      strcpy(buf,"\\\"");
    } else {
      buf[0]=ch;
      buf[1]=0;
    }
    return buf;
  };
};

qchar_t qchar;
string quote(const string &str) {
  string res="\"";
  auto b(str.begin()), e(str.end());
  while(b!=e)
    res+=qchar(*b++);
  res+="\"";
  return res;
}
