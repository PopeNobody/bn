#pragma once
#include "std.hh"


inline ostream &operator<<(ostream &lhs, const type_info & rhs){
  return lhs << demangle(rhs.name());
}
inline ostream &operator<<(ostream &lhs, const source_location &rhs){
  lhs << rhs.file_name() << ":" << rhs.line() << ":" << rhs.function_name();
  return lhs;
};
inline ostream &operator<<(ostream &lhs, const exception &rhs) {
  return lhs << "ex: type=" << typeid(rhs) << " what=" << rhs.what();
}
template<class T, class U>
std::ostream &operator<<(ostream &os, const std::pair<T, U> &x) {
  os << "(" << x.first << ", " << x.second << ")";
  return os;
}

struct streamer_b {
  virtual ostream& write(ostream &) const = 0;
};
inline ostream &operator<<(ostream &lhs, const streamer_b &rhs) {
  return rhs.write(lhs);
}
inline ostream &operator<<(ostream &lhs, const std::strong_ordering &rhs) {
  const char *str=0;
  if(rhs<0) {
    str="less";
  } else if ( rhs > 0 ) {
    str="greater";
  } else {
    str="equal";
  };
  return lhs;//.stream(lhs);
}
using sloc_t=std::source_location;
template<typename val_t>
struct streamer_t : public streamer_b {
  const val_t &rhs;
  streamer_t(const val_t&rhs)
    : rhs(rhs)
  {
  }
  virtual ostream &write(ostream &lhs) const {
    return lhs << rhs;
  }
};
void do__show(const char *text, const streamer_b &val,
    const sloc_t &sloc);
template<typename val_t>
inline void __show(const char *text, const val_t &val,
    const sloc_t & sloc)
{
  streamer_t<val_t> str(val);
  do__show(text,str,sloc);
}

static bool verbose=true;
#define show(x) do{ \
  if(verbose) \
    __show(#x,(x), std::source_location::current()); \
  } while(false)
#ifdef assert
#undef assert
#endif
#define assert xassert
