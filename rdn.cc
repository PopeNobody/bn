#include "std.hh"
#include <cmath>
#include "lilarr.hh"
#include "sl_t.hh"
#include "xassert.hh"
#include "dn.hh"

#ifdef assert
#undef assert
#endif
#define assert xassert


template<typename _type>
struct sign_t;
template<typename _type>
struct next_t;

template<> struct next_t< int32_t> { typedef int64_t type; };
template<> struct next_t< int16_t> { typedef int32_t type; };
template<> struct next_t< int8_t>  { typedef int16_t type; };

template<> struct next_t<uint8_t>  { typedef uint16_t type; };
template<> struct next_t<uint16_t> { typedef uint32_t type; };
template<> struct next_t<uint32_t> { typedef uint64_t type; };

template<> struct sign_t<uint64_t> {
  static constexpr bool has_sign = false; 
  typedef int64_t sint;
  typedef uint64_t uint;
};
template<> struct sign_t< int64_t> {
  static constexpr bool has_sign = true; 
  typedef int64_t sint;
  typedef uint64_t uint;
};
template<> struct sign_t< int32_t> {
  static constexpr bool has_sign = true; 
  typedef int32_t sint;
  typedef uint32_t uint;
};
template<> struct sign_t< int16_t> {
  static constexpr bool has_sign = true; 
  typedef int16_t sint;
  typedef uint16_t uint;
};
template<> struct sign_t< int8_t>  {
  static constexpr bool has_sign = true; 
  typedef int8_t sint;
  typedef uint8_t uint;
};

template<> struct sign_t<uint8_t>  {
  static constexpr bool has_sign = false; 
  typedef int8_t sint;
  typedef uint8_t uint;
};
template<> struct sign_t<uint16_t> {
  static constexpr bool has_sign = false; 
  typedef int16_t sint;
  typedef uint16_t uint;
};


template<typename _type>
struct int_traits_t
{
  typedef _type type;
  typedef int_traits_t<type> self;
  typedef typename next_t<type>::type next;
  typedef typename next_t<next>::type nextnext;
  
  typedef typename sign_t<next>::sint sintnext;
  typedef typename sign_t<next>::uint uintnext;
  
  // one of these will be us.
  typedef typename sign_t<type>::sint sint;
  typedef typename sign_t<type>::uint uint;

  static constexpr type mask = (type)-1;
  static constexpr next oflow = next(mask)+1;

  static constexpr size_t bin_wid = 8*sizeof(type);
  static constexpr size_t hex_wid = bin_wid/4;
};

auto find_hex(const string & val)
{
  auto beg(val.begin()), end(val.end());
  while(beg!=end && isspace(*beg))
    ++beg;
  while(beg!=end && isspace(*(end-1)))
    --end;
  if(end-beg >1 && beg[0]=='0' && beg[1]=='x')
    beg+=2;
  if(beg==end)
    throwRe("no hex number found in: " << val);
  while(beg!=end && *beg=='0')
    beg++;
  auto pos=beg;
  while(beg!=end) {
    if(!isxdigit(*beg++)) {
      throwRe("bad data found at pos(" << (beg-val.begin()) << ") in str: "
          << val);
    };
  }
  return make_pair(
      make_reverse_iterator(&*end),
      make_reverse_iterator(&*pos));
}
//   dn_t operator-(const dn_t &lhs, const dn_t &rhs) {
//     dn_t res=lhs;
//     dn_t::tmp_t tmp=0;
//     size_t pos;
//     for(pos=0;pos<rhs.size();pos++) {
//       if(pos<lhs.size())
//         tmp+=lhs.dat[pos];
//       tmp-=rhs.dat[pos];
//       res.dat[pos]=tmp&dn_t::mask;
//       tmp>>=dn_t::bin_wid;
//     }
//     //     show(tmp);
//     if(tmp)
//       res.dat.push_back(tmp&dn_t::mask);
//   
//     res.normalize();
//     return res;
//   }
//   dn_t::operator bool() const {
//     auto b(dat.begin()), e(dat.end());
//     if(b==e)
//       return false;
//     do {
//       if(*b)
//         return false;
//     } while(++b!=e);
//     return true;
//   }

int xmain();
static const string str1 = string("0x0100");

template<typename num_t>
  void add_check(num_t &x, const num_t &z)
  {
    cout << __PRETTY_FUNCTION__ << endl;
    auto tmp=x+z;
    if(tmp-z!=x) {
      show(x);
      show(z);
      show(tmp);
      show(x+z);
      show(x+z-z);
      throw runtime_error("overflow (or err)");
    }
    x=tmp;
  };

void tmp_test() {
  dn_t  x    =  0x0ffffff0  ;
  dn_t  z    =  0x01ff      ;
  dn_t  tmp  =  x+z         ;
  dn_t  chk  =  tmp-z       ;
  assert(x==chk);
}
int sub_test() {
  uint64_t X=0xffffff0;
  dn_t x=X;
  uint64_t Z=0x1ff;
  dn_t z=Z;
  for(int i=1;i<100;i++) {
    {
      bool verbose=false;
      show(i);
      show(X);
      show(Z);
      show(x);
      show(z);
      add_check(X,Z);
      add_check(x,z);
    }
    try { 
      assert_op(x,==,X);
      auto T=Z;
      auto A=2*Z;
      z=Z=T+A;
      
    } catch ( ... ) {
      show(log(Z)/log(2));
      show(i);
      show(x);
      show(X);
      X+Z;
      x+z;
      break;
    }
  };
  return 0;
}
int main() {
  tmp_test();
  return xmain();
};
void str_tests();
void op_tests();
int xmain() {
  cout << hex << showbase << boolalpha;
  str_tests();
  op_tests();
  return 0;
}
void op_tests()
{
}
struct case_t {
  uint32_t init;
  char str[16];
  size_t size;
};
vector<case_t> cases = {
  { 0, "0x00", 0 },
  { 1, "0x01", 1 },
  {  0x10,        "0x10",        1  },
  {  0x100,       "0x0100",      2  },
  {  0x1000,      "0x1000",      2  },
  {  0x10000,     "0x010000",    3  },
  {  0x100000,    "0x100000",    3  },
  {  0x1000000,   "0x01000000",  4  },
  {  0x10000000,  "0x10000000",  4  },
  {  0x1000000f,  "0x1000000f",  4  },
  {  0xdeadbeef,  "0xdeadbeef",  4  },
  {  0xbaadf00d,  "0xbaadf00d",  4  },
};
ostream &operator<<(ostream &lhs, const case_t &rhs) {
  lhs  << setw(sizeof(rhs.str)) << rhs.init
       << setw(sizeof(rhs.str)) << rhs.str
       << setw(3) << rhs.size;
  return lhs;
}
void str_tests() {
  dn_t def;
  for( auto c : cases ) {
    dn_t bn(c.init);
    assert((c.init <=> bn)==0);
    assert_op(c.str,==,bn.str());
    dn_t sn(c.str);
    assert_op(bn.str(),==,sn.str());
    assert(bn.str()==sn.str());
    assert_op(bn,==,sn);
    assert(bn<=>sn==0);
    assert(bn==sn);
    assert(bn<0 == false);
    if(c.init) {
      assert(def != bn);
      assert(bn>0);
      assert(bn>=0);
      assert(bn<=0 == false);
    } else {
      assert(def == bn);
      assert(bn>0 == false);
      assert(bn>=0);
      assert(bn<0 == false);
      assert(bn<=0);
      assert(bn>=0);
    }
  }
}
