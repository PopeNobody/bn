#pragma once
// spaces before std headers are intentional.
// +1,$!LC_COLLATE=C sort -u
#define __assert_fail __hide_assert_fail
#include <assert.h>
#undef __assert_fail
#include <algorithm>
#include <cstddef>
#include <cstring>
#include <cmath>
#include <experimental/source_location>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <sstream>
#include <stdexcept>
#include <vector>
#include<boost/core/demangle.hpp>
#include<boost/array.hpp>
namespace std {
  using experimental::source_location;
}
using boost::array;
using boost::core::demangle;
using std::boolalpha;
using std::cerr;
using std::cout;
using std::endl;
using std::exception;
using std::hex;
using std::make_reverse_iterator;
using std::max;
using std::min;
using std::ostream;
using std::ostringstream;
using std::runtime_error;
using std::setfill;
using std::setw;
using std::showbase;
using std::source_location;
using std::string;
using std::strong_ordering;
using std::type_info;
using std::vector;

#include "xassert.hh"
