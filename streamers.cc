#include "streamers.hh"
#include "quote.hh"

string last_func;
void do__show(const char *text, const streamer_b &val,
    const sloc_t &sloc
    ) {
  std::ostringstream str;
  if(sloc.function_name()!=last_func) {
    last_func=sloc.function_name();
    str 
      << sloc.file_name() 
      << ":" << sloc.line() 
      << ":" << sloc.function_name()
      << "\n";
  };
  str << sloc.file_name() << ":" << sloc.line() << ":";
  str << hex << showbase << boolalpha;
  str << std::left << setw(20) << text << val;
  std::cout << quote(str.str()) << std::endl;
};
