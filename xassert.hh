#pragma once
#include"sl_t.hh"
void __xassert_fail(
    const char *cond, sl_t sl=sl_t()
    ) 
  __attribute__((noreturn));
void __xassert_fail(const char *cond, const char *file, unsigned line, 
    const char * func)
  __attribute__((noreturn));

#define xassert(x, msg...) do{ \
  if(!(x)) { __xassert_fail(#x); }; \
} while(0);
#define assert_op( x, op, y ) do { \
  if( x op y ) \
    break; \
  cerr << "assertion failed" << endl; \
  cerr << sl_t() << endl; \
  show(x); \
  show(y); \
  assert( x op y ); \
} while(0)
#include "streamers.hh"
#define throwRe(x) do{ \
  ostringstream str; \
  str << x; \
  throw runtime_error(str.str()); \
} while(0)
